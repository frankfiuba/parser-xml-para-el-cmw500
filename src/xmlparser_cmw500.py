#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 26 11:01:43 2019

@author: frank
"""


import xml.etree.ElementTree as ET

import pandas as pd
import numpy as np

from matplotlib import pyplot as plt

    
class XmlCmw500:
    """ Clase que representa a los XML provenientes del software CMW500Run."""
    
    def __init__(self, nameXmlFile: str):
        
        if not isinstance(nameXmlFile, str):
            raise ValueError("El argumento debe ser un str.")
            
        try:
            self.tree = ET.parse(nameXmlFile)
        
        except Exception as e:
            raise
            
        self.root = self.tree.getroot()
    
    def _getXmlElements(self, name: str, typeName: str, title: str, matchExactly=False):
        
        listElem = []
        
        for elem in self.root.iter(typeName):
    
            # find() busca sólo los tag que son hijos DIRECTOS del elemento actual
             tituloTablaElem = elem.find(title).text
             
             if tituloTablaElem is not None:
                 if matchExactly:
                     if name == tituloTablaElem:
                         listElem.append(elem)
                 else:
                     if name in tituloTablaElem:
                         listElem.append(elem)
                         
        return listElem
    
    
    def getXmlTable(self, name, matchExactly=False):
        """ Busca la tabla 'name' en el xml. Si matchExactly es False, devuelve
        la primer tabla cuyo nombre incluya 'name'. Si matchExactly es True,
        devuelve la primer tabla cuyo nombre coincida exactamente con 'name'.
        Si no se encuentra ninguna tabla, devuelve None.
        """
        return self._getXmlElements(name, 'TestItemList', 'ListContext', matchExactly)
        
    
    def getXmlDiagram(self, name, matchExactly=False):
        """ Busca y devuelve un Diagrama con el nombre 'name' en el XML. """
        
        return self._getXmlElements(name, 'Diagram', 'Name', matchExactly)
    
class ParserXmlTable:
    """ Clase que se encarga de parsear una tabla en XML y devolver un tipo de
    datos diferente con la misma información.
    """
    def __init__(self, xmlTable):
        
        if not isinstance(xmlTable, ET.Element):
            raise ValueError("El argumento debe ser del tipo xml.etree.ElementTree.Element,"
                             "no {}".format(str(type(xmlTable))))
            
        self.xmlTable = xmlTable
    
    def getDataframe(self):
        """ Devuelve un DataFrame de la tabla XML, con todas las
        filas y columnas tal cual están presentes en el XML.
        """
        listNameColumns = [elementCol.tag for elementCol in self.xmlTable.find('ItemContext').getchildren()]
        
        listTable = []
        
        # Agrego como primer fila de la tabla, el header de la tabla XML
        rowList = []
        for i, cellItem in enumerate(self.xmlTable.find('ItemContext').getchildren()):
        
            rowList.append(cellItem.text)
        # Lo agrego a la lista-tabla
        listTable.append(rowList)
        
        # Agrego el resto de las filas de la tabla
        for i, rowItem in enumerate(self.xmlTable.findall('TestItem')):
            
            rowList = []
            # Armo una fila con este for y lo agrego a la lista-tabla
            for j, cellItem in enumerate(rowItem.getchildren()):
            
                rowList.append(cellItem.text)
            
            listTable.append(rowList)
        
        # Creo y devuelvo el DataFrame
        df = pd.DataFrame(listTable, columns = listNameColumns)
        
        return df

class ParserXmlDiagram:
    
    def __init__(self, xmlDiagram):
        
        if not isinstance(xmlDiagram, ET.Element):
            raise ValueError("El argumento debe ser del tipo xml.etree.ElementTree.Element,"
                             "no {}".format(str(type(xmlDiagram))))
            
        self.xmlDiagram = xmlDiagram
    
    def getDiagram(self):
        """ Devuelve un objeto Diagram del diagrama XML, con todas los
        datos, trazos, etc.
        """
        # Leo el nombre del diagrama
        diagramName = self.xmlDiagram.find('Name').text
        
        # Extraigo los datos del X axis
        xmin = self.xmlDiagram.find('XAxis').find('Min').text
        xmax = self.xmlDiagram.find('XAxis').find('Max').text
        xunit = self.xmlDiagram.find('XAxis').find('Unit').text
        
        xAxis = Axis(float(xmin), float(xmax), xunit)
        
        # Extraigo los datos del Y axis
        ymin = self.xmlDiagram.find('YAxis').find('Min').text
        ymax = self.xmlDiagram.find('YAxis').find('Max').text
        yunit = self.xmlDiagram.find('YAxis').find('Unit').text
        
        yAxis = Axis(float(ymin), float(ymax), yunit)
        
        # Creo el Diagrama
        diagram = Diagram(diagramName, xAxis, yAxis)
        
        # Agrego los trazos del diagrama XML al diagrama creado
        for trace in self.xmlDiagram.findall('Trace'):
    
            traceName = trace.find('title').text
            
            xValues = np.fromstring(trace.find('XValues').text,sep=',')
            yValues = np.fromstring(trace.find('YValues').text,sep=',')
                
            objectTrace = Trace(traceName, xValues, yValues)
            
            diagram.traces.append(objectTrace)
        
        return diagram        
        
        
class Diagram:
    """ Clase que representa a un gráfico que puede tener varios trazos."""
    
    def __init__(self, name, xaxis, yaxis):
        
        self.name = name
        self.xAxis = xaxis
        self.yAxis = yaxis
        self.traces = []
    
    def saveFig(self, filename):
        
        plt.close('all')
        plt.figure(0)
        
        bufferTracesTitles = []
        bufferTracesColors = []
        
        for trace in self.traces:
            if trace.title not in bufferTracesTitles:
                # Agrego el titulo y el color del trazo al buffer
                bufferTracesTitles.append(trace.title)
                
                auxPlot = plt.plot(trace.xValues,
                         trace.yValues,
                         label=trace.title)
                
                colorTrace = auxPlot[-1].get_color()
                
                bufferTracesColors.append(colorTrace)
                
            else:
                # Busco el indice del nombre del trazo en el buffer
                # para obtener el color del trazo
                indexColor = bufferTracesTitles.index(trace.title)
                plt.plot(trace.xValues,
                         trace.yValues,
                         color=bufferTracesColors[indexColor])
                
        plt.xlim(self.xAxis.minValue, self.xAxis.maxValue)
        plt.ylim(self.yAxis.minValue, self.yAxis.maxValue)
        
        plt.xlabel(self.xAxis.unit)
        plt.ylabel(self.yAxis.unit)
        
        plt.title(self.name)
        
        plt.legend(loc='upper right', prop={'size': 8})
        
        
#        plt.show()
#        
#        manager = plt.get_current_fig_manager()
#        manager.window.showMaximized()
#        
#        #plt.tight_layout()
#        
        plt.savefig(filename)
        

    
    @property
    def xAxis(self):
    
        return self._xAxis
    
    @xAxis.setter
    def xAxis(self, value):
        
        if not isinstance(value, Axis):
    
            raise TypeError("El valor recibido es del tipo {} y se espera Axis".format(str(type(value))))
            
        self._xAxis = value
    
    @property
    def yAxis(self):
    
        return self._yAxis
    
    @yAxis.setter
    def yAxis(self, value):
        
        if not isinstance(value, Axis):
    
            raise TypeError("El valor recibido es del tipo {} y se espera Axis".format(str(type(value))))
            
        self._yAxis = value
    
    @property
    def name(self):
    
        return self._name
    
    @name.setter
    def name(self, value):
        
        if not isinstance(value, str):
    
            raise TypeError("El valor recibido es del tipo {} y se espera str".format(str(type(value))))
            
        self._name = value
    
        
class Axis:
    """Clase que contiene información sobre los ejes de un diagrama. """
    def __init__(self, minValue, maxValue, unit, stepSize=None, decimalDigits=None):
        
        self.minValue = minValue
        self.maxValue = maxValue
        self.unit = unit
        self.stepSize = stepSize
        self.decimalDigits = decimalDigits
    
    @property
    def title(self):
    
        return self._title
    
    @title.setter
    def title(self, value):
        
        if not isinstance(value, str):
    
            raise TypeError("El valor recibido es del tipo {} y se espera str".format(str(type(value))))
            
        self._title = value
    
    @property
    def minValue(self):
    
        return self._minValue
    
    @minValue.setter
    def minValue(self, value):
        
        if not isinstance(value, float):
    
            raise TypeError("El valor recibido es del tipo {} y se espera float".format(str(type(value))))
            
        self._minValue = value
    
    @property
    def maxValue(self):
    
        return self._maxValue
    
    @maxValue.setter
    def maxValue(self, value):
        
        if not isinstance(value, float):
    
            raise TypeError("El valor recibido es del tipo {} y se espera float".format(str(type(value))))
            
        self._maxValue = value
    
    @property
    def unit(self):
    
        return self._unit
    
    @unit.setter
    def unit(self, value):
        
        if not isinstance(value, str):
    
            raise TypeError("El valor recibido es del tipo {} y se espera str".format(str(type(value))))
            
        self._unit = value
    
    
class Trace:
    """ Clase que representa a un trazo en dos dimensiones. """
    
    def __init__(self, title, xValues, yValues):
        
        self.title = title
        self.xValues = xValues
        self.yValues = yValues
    
    @property
    def title(self):
    
        return self._title
    
    @title.setter
    def title(self, value):
        
        if not isinstance(value, str):
    
            raise TypeError("El valor recibido es del tipo {} y se espera str".format(str(type(value))))
            
        self._title = value
    
    @property
    def xValues(self):
    
        return self._xValues
    
    @xValues.setter
    def xValues(self, value):
        
        if not isinstance(value, np.ndarray):
    
            raise TypeError("El valor recibido es del tipo {} y se espera np.array".format(str(type(value))))
            
        self._xValues = value
    
    @property
    def yValues(self):
    
        return self._yValues
    
    @yValues.setter
    def yValues(self, value):
        
        if not isinstance(value, np.ndarray):
    
            raise TypeError("El valor recibido es del tipo {} y se espera np.ndarray".format(str(type(value))))
            
        self._yValues = value
    
    
    
    
    