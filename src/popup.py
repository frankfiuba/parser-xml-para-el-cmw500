from PyQt5.QtWidgets import QMessageBox
from PyQt5 import QtWidgets

def showErrorMsg(message):

    msg = QMessageBox()
    msg.setIcon(QMessageBox.Warning)

    textMsg = "{}".format(message)

    msg.setText(textMsg)

    msg.setWindowTitle("¡Error!")

    msg.setStandardButtons(QMessageBox.Ok)


    return msg.exec_()