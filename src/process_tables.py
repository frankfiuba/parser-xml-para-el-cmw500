import translate

###############################################################################
# Funciones de procesamiento de tablas en dataframes
###############################################################################
def processDfForMaxOutPower(dfRaw):
    """Elimino columnas innecesarias y uno 2 columnas(Description & Condition).
    Hago la traducción del inglés a español.
    """
    dfProcessed = dfRaw.drop(["No","VerdictInfo","Unit"], axis=1)
    
    # Concateno las columnas 'Description' y 'Condition'
    # en una sola, en la columna 'Condition'
    dfProcessed['Condition'] = dfProcessed['Description'] + ":" + dfProcessed['Condition']
    
    # Elimino la columna 'Description'
    dfProcessed.drop("Description", axis=1, inplace=True)
    
    # TRADUZCO PALABRAS DE LA TABLA
    dfProcessed = translate.translateDf(dfProcessed, translate.TRANSLATION_DICT_DBM)
    
    return dfProcessed

def processOccupiedBW(dfRaw):
    
    dfProcessed = dfRaw.drop(["No","VerdictInfo","Unit","LowLimit"], axis=1)
    
    # Concateno las columnas 'Description' y 'Condition'
    # en una sola, en la columna 'Condition'
    dfProcessed['Condition'] = dfProcessed['Description'] + ":" + dfProcessed['Condition']
    
    # Elimino la columna 'Description'
    dfProcessed.drop("Description", axis=1, inplace=True)
    
    # TRADUZCO PALABRAS DE LA TABLA
    dfProcessed = translate.translateDf(dfProcessed, translate.TRANSLATION_DICT_MHZ)
 
    return dfProcessed

def processSpectrumEmissionMask(dfRaw):
    # Por ahora es la misma que para potencia
    df = processDfForMaxOutPower(dfRaw)
    # Pero sin la columna de limite inferior
    df.drop("LowLimit", axis=1, inplace=True)
    
    return df

def processAdjacentChannel(dfRaw):
    # Por ahora es la misma que para potencia
    df = processDfForMaxOutPower(dfRaw)
    # Pero sin la columna de limite inferior
    df.drop("LowLimit", axis=1, inplace=True)
    
    return df

def processFrequencyError(dfRaw):
    # Por ahora es la misma que para potencia
    return processDfForMaxOutPower(dfRaw)

def processRefSensitivityLevel(dfRaw):
    # Por ahora es la misma que para potencia
    df = processDfForMaxOutPower(dfRaw)
    # Pero sin la columna de limite inferior
    df.drop("LowLimit", axis=1, inplace=True)
    
    return df

###############################################################################
###############################################################################