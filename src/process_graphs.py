""" En este módulo se procesan diagramas presentes en el xml, es decir, se traducen los nombres
de los trazos presentes y se modifican, en caso de ser necesario, los niveles de dichos trazos.
"""
import re

def processDiagramSpectrumMask(diagram):
    """ Traduce los nombres del diagrama y agrega un offset a las máscaras si
    es necesario. Puede eliminar algunos trazos también.
    """
    if "Spectrum Emission Mask Band" in diagram.name:
        diagram.name = diagram.name.replace("Spectrum Emission Mask Band", "Máscara de Transmisión Banda")
   
    for trace in diagram.traces:
    
        # Agrego un offset
        if 'Spectrum Emission Mask Limits' in trace.title:
                    trace.yValues -= 1.5
                    trace.title = 'Límite Máscara de Transmisión'
        if 'Filter' in trace.title:
            # Expresion regular para traducir Filtro y cambiar de orden la frase
             trace.title = re.sub("(.*)(Filter)",r"Filtro \1",trace.title)
    
    return diagram