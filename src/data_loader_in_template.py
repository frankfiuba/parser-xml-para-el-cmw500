#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 13:52:03 2019

@author: frank

Script para crear un documento word.
En el documento se creará una tabla a partir de un xml, inicialmente
la tabla de Maximum Output Power
En ese documento se debe insertar la tabla debajo del parráfo que dice
"Potencia máxima conducida"
"""
import os
import re

import docx
from docx.shared import Cm,Pt
from docx.enum.table import WD_TABLE_ALIGNMENT,WD_CELL_VERTICAL_ALIGNMENT
from docx.enum.style import WD_STYLE_TYPE
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT

import pandas as pd

from xmlparser_cmw500 import XmlCmw500, ParserXmlTable, ParserXmlDiagram
import popup
import translate

###############################################################################
###############################################################################

def getParagraphWithText(doc, string: str, matchExactly=False):
    
    for paragraph in doc.paragraphs:
        
        if matchExactly:
            
            if paragraph.text == string:
                return paragraph
        
        else:
            if string in paragraph.text:
                return paragraph
    
    return None

def generateWordTable(doc, df):
    """ Devuelve una tabla word con los anchos de columna
    definidos apropiadamente.
    """
    table = doc.add_table(0,len(df.columns))
    
    # Agrego las columnas con el ancho deseado de cada una
#    table.columns[0].width = Cm(5)
#    table.columns[1].width = (Cm(1.5))
#    table.columns[2].width = (Cm(1.5))
#    table.columns[3].width = (Cm(1.75))
#    table.columns[4].width = (Cm(1.5))
#    table.columns[5].width = (Cm(1.5))
    
    # Genero la tabla a partir del dataframe
    
    _generateTableFromDataframe(table, df)
    
    try:
        table.style = "Table Grid"
        table.alignment = WD_TABLE_ALIGNMENT.CENTER
        table.style.font.name = "Arial"
        table.style.font.size = Pt(8)
    #    table.autofit = False
    #    table.autofit = True
    
    except KeyError as e:
        
        popup.showErrorMsg("No se puede cargar el estilo Table Grid: {}".format(str(e)))
    
    return table

def _generateTableFromDataframe(table, df):
    """ Genera automaticamente la tabla a partir del dataframe."""
    
    for i in range(len(df.index)):
        
        rowTable = table.add_row()
        
        for j in range(len(df.columns)):
            cell = rowTable.cells[j]
            cell.text = str(df.iloc[i,j])
            par = rowTable.cells[j].paragraphs[0]
            par.style.font.name = "Arial"
            par.style.font.size = Pt(10)
            par.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
            cell.vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
            
def moveTableAfterParagraph(table, paragraph):
    
    paragraphBefore = paragraph.insert_paragraph_before("")
    tbl, p = table._tbl, paragraphBefore._p
    p.addnext(tbl)

###############################################################################
# Obtengo la tabla del XML y genero un dataframe con los datos que quiero mostrar
###############################################################################

def dataLoadInTemplate(nameXml, nameTemplate, parameters, pahtProject, mantenerMarcadores=False):

    # Empiezo por extraer la información de la tabla xml
    xmlCmw500 = XmlCmw500(nameXml)

    document = docx.Document(nameTemplate)

    # Parametros
    listTableNames = parameters.listTableNames
    listFunctionsProcessDfTables = parameters.listFunctionsProcessDfTables
    listFunctionsProcessDiagrams = parameters.listFunctionsProcessDiagrams
    listParagraphMarks = parameters.listParagraphMarks
    listDiagramNames = parameters.listDiagramNames
    listImagesMarks = parameters.listImagesMarks

    ###############################################################################
    # Proceso las tablas
    ###############################################################################

    for i, tableName in enumerate(listTableNames):
        
        listXmlTable = xmlCmw500.getXmlTable(tableName)
        
        # Si la lista de diagramas encontradas es vacía, continúo con otro ...
        if not bool(listXmlTable):
            continue
        
        # Como pueden haber varias tablas en el xml con el mismo nombre, itero
        for j, xmlTable in enumerate(listXmlTable):
            
            parserTable = ParserXmlTable(xmlTable)
            
            # Obtengo el dataframe
            dfRaw = parserTable.getDataframe()            
            
            # Proceso el dataframe para obtener la tabla que deseo mostrar
            dfFinal = listFunctionsProcessDfTables[i](dfRaw)
            
            # Busco el párrafo donde está el marcador
            paragraph = getParagraphWithText(document, listParagraphMarks[i])
            
            if paragraph is None:
                
                popup.showErrorMsg("No se encontró un párrafo en la plantilla con el texto {}".format(listParagraphMarks[i]))
                continue
            else:
                
                # Agrego la tabla al final del documento y luego la muevo 
                # al lugar del marcador                                
                tabla = generateWordTable(document, dfFinal)
                
                # Muevo la tabla al párrafo 'paragraph'
                moveTableAfterParagraph(tabla, paragraph)

        
        # Borro el texto del párrafo
        if paragraph is not None:
            
            if mantenerMarcadores is False:
                    paragraph.clear()
    
            #paragraph.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER

    ###############################################################################
    # Proceso los diagramas
    ###############################################################################
    # Creo el directorio img
    if not os.path.exists(pahtProject + "/img"):

        os.mkdir(pahtProject + "/img")
    
    for i, diagramName in enumerate(listDiagramNames):        
        
        listXmlDiagrams = xmlCmw500.getXmlDiagram(diagramName)
        
        # Si la lista de diagramas encontradas es vacía, continúo...
        if not bool(listXmlDiagrams):
            continue
        
        # Agrego la tabla donde se pegaran las imagenes en dos columnas
        tableImg = document.add_table(0, 2)
        
        # Busco el párrafo donde está el marcador
        paragraph = getParagraphWithText(document, listImagesMarks[i])
        
        # Como pueden haber varios diagramas en el xml con el mismo nombre, itero
        for j, xmlDiagram in enumerate(listXmlDiagrams):
            
            parserDiagram = ParserXmlDiagram(xmlDiagram)
            
            # Obtengo el objeto diagrama
            diagram = parserDiagram.getDiagram()            
            
            # Proceso el diagrama para obtener la imagen que deseo mostrar
            finalDiagram = listFunctionsProcessDiagrams[i](diagram)
            
            # Guardo la imagen en la carpeta ./img
            nameImage = pahtProject + "/img/{}_{}.png".format(diagramName,j)
            
            finalDiagram.saveFig(nameImage)        
            
            if paragraph is None:
                
                popup.showErrorMsg("No se encontró un párrafo en la plantilla con el texto {}".format(listImagesMarks[i]))
                break
            else:
                
                # Agrego descripcion/titulo de imagen
                
                if not (j % 2):
                    tableImg.add_row()
                    tableImg.add_row()
                    #rowTableImg = tableImg.add_row()
                
                numRow = int(j/2)*2 + 1
                numColumn = j % 2
                
                # Agrego la imagen
                paragraphImg = tableImg.row_cells(numRow)[numColumn].paragraphs[0]
                runP = paragraphImg.add_run("")
                runP.add_picture(nameImage,Cm(7.5),Cm(5))
                paragraphImg.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
        
        if paragraph is not None:
            # Muevo la tabla de imagenes al párrafo 'paragraph'
            moveTableAfterParagraph(tableImg, paragraph)
            # Borro el texto del párrafo.
            if mantenerMarcadores is False:
                    paragraph.clear()
                
                

        
        # Borro el texto del párrafo
        #paragraph.clear()
        #runP = paragraph.add_run("\n")
        #runP.add_picture("capturaNPulsos.png",Cm(10),Cm(7))
        #paragraph.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER

    nameFileout = pahtProject + "/report.docx"

    document.save(nameFileout)
        
    return nameFileout