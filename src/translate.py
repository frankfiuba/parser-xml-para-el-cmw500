""" En este módulo se proveen las funciones de traducción de textos presentes
en las celdas de los distintos dataframes para cada tecnología.
"""

TRANSLATION_DICT_DBM = {'Lower Limit':'Límite Inferior [dBm]',
                    'Band': 'Banda',
                    'Not Required for 3GPP default':'No requerido por defecto en 3GPP',
                    'Upper Limit':'Límite Superior [dBm]',
                    'Measured': 'Medido [dBm]',
                    'Status': 'Cumple (Sí/No)',
                    'Failed': 'No',
                    'Passed': 'Sí',
                    'Unit': 'Unidad',
                    'Test Item': 'Item',
                    'Margin from': 'Margen desde',
                    ' to ': ' a '
                    }

TRANSLATION_DICT_MHZ = {'Lower Limit':'Límite Inferior [MHz]',
                    'Band': 'Banda',
                    'Not Required for 3GPP default':'No requerido por defecto en 3GPP',
                    'Upper Limit':'Límite Superior [MHz]',
                    'Measured': 'Medido [MHz]',
                    'Status': 'Cumple (Sí/No)',
                    'Failed': 'No',
                    'Passed': 'Sí',
                    'Unit': 'Unidad',
                    'Test Item': 'Item',
                    'Margin from': 'Margen desde',
                    ' to ': ' a '
                    }

def translateDf(df, translationDict):
    """ Traduce el contenido del dataframe segun el diccionario
    translationDict y devuelve el dataframe traducido.
    """
    
    dfTranslated = df.replace(translationDict, regex=True)
    
    return dfTranslated