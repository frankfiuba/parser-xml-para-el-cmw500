import sys
import os
from PyQt5 import QtWidgets, uic, QtCore, QtGui

import data_loader_in_template
import parameters
import popup
from shared import ReportStatus

class UiParserCmw500:
    """ Clase que represente a la interfaz gráfica de usuario
    """
    THISDIR = os.path.dirname(os.path.abspath(__file__))

    # @classmethod
    # def fromJson(cls):
    #     pass

    def __init__(self):

        self.ui = uic.loadUi(os.path.join(self.THISDIR,"ui.ui"))
        
        self.projectPath = None

        self.nameXmlFile = None

        self.nameTemplateFile = None

        self.ui.pushButtonElegirCarpeta.clicked.connect(self.setFolderProject)

        self.ui.pushButtonCargarArchivoXML.clicked.connect(self.setXmlFile)

        self.ui.pushButtonCargarPlantillaDocx.clicked.connect(self.setTemplateFile)

        self.ui.pushButtonGenerarInforme.clicked.connect(self.generateReport)

        self.setReportStatus(ReportStatus.NO_GENERADO)

    def setReportStatus(self, status):

        if status == ReportStatus.NO_GENERADO:

            self.ui.labelEstadoInforme.setText(ReportStatus.NO_GENERADO.name)
            self.ui.labelEstadoInforme.setStyleSheet(" background-color : red;")
        
        elif status == ReportStatus.GENERADO:

            self.ui.labelEstadoInforme.setText(ReportStatus.GENERADO.name)
            self.ui.labelEstadoInforme.setStyleSheet(" background-color : green;")
    
    def setFolderProject(self):

        dirName = QtWidgets.QFileDialog.getExistingDirectory()

        if dirName:
            
            self.projectPath = dirName

            self.ui.labelNombreCarpeta.setText(self.projectPath)

    def setXmlFile(self):
        
        nameFile = QtWidgets.QFileDialog.getOpenFileName(filter="*.xml")[0]
        # if a file is selected
        if nameFile:
            
            self.nameXmlFile = nameFile
            self.ui.labelNombreArchivoXML.setText(nameFile)

    def setTemplateFile(self):
        
        nameFile = QtWidgets.QFileDialog.getOpenFileName(filter="*.docx")[0]
        # if a file is selected
        if nameFile:
            
            self.nameTemplateFile = nameFile
            self.ui.labelNombrePlantillaDocx.setText(nameFile)

    def generateReport(self):

        params = None
        
        flagMarcadores = False
        
        if self.ui.checkBoxMantenerMarcadores.isChecked():
            flagMarcadores = True

        if self.ui.comboBoxTecnologia.currentText() == "UMTS":

            params = parameters.UMTSParameters

            try:
                nameReport = data_loader_in_template.dataLoadInTemplate(self.nameXmlFile,
                                                                        self.nameTemplateFile,
                                                                        params,
                                                                        self.projectPath,
                                                                        mantenerMarcadores=flagMarcadores)
            
            except Exception as e:

                popup.showErrorMsg(str(e))
                return

            self.setReportStatus(ReportStatus.GENERADO)
            self.ui.labelRutaInforme.setText(nameReport)
        else:
            popup.showErrorMsg("No implementado!")
    
    # def toJson(self):
    #     pass

if __name__ == '__main__':

    QtWidgets.QApplication.setStyle(QtWidgets.QStyleFactory.create("Fusion"))
    app = QtWidgets.QApplication(sys.argv)

    pluginParserCmw500 = UiParserCmw500()

    dmw = QtWidgets.QMainWindow()
    dmw.setCentralWidget(pluginParserCmw500.ui)
    dmw.show()

    try:
        sys.exit(app.exec_())

    except SystemExit as e:

        del app, dmw