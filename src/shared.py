""" En este módulo se encuentran enums y typos de datos compartidos por todo el proyecto.
"""
import enum

class ReportStatus(enum.Enum):

    NO_GENERADO = 0
    GENERADO = 1

class Technology(enum.Enum):

    UMTS = 0
    GSM = 1
    LTE = 2