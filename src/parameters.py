""" En este módulo se encuentran funciones que devuelven una serie de 
parámetros en función de una tecnología
"""
import process_tables
import process_graphs
import abc

class UMTSParameters:
    # Nombres de las tablas del xml que me interesan
    listTableNames = ["Maximum Output Power",
                        "Occupied Bandwidth",
                        "Spectrum Emission Mask",
                        "Adjacent Channel Leakage Power Ratio",
                        "Frequency Error",
                        "Reference Sensitivity Level"
                        ]
    # Diagramas del xml que me interesan
    listDiagramNames = ["Spectrum Emission Mask Band"]

    # Lista de punteros a función que procesan los dataframes de las tablas extraidas del xml
    listFunctionsProcessDfTables = [process_tables.processDfForMaxOutPower,
                                    process_tables.processOccupiedBW,
                                    process_tables.processSpectrumEmissionMask,
                                    process_tables.processAdjacentChannel,
                                    process_tables.processFrequencyError,
                                    process_tables.processRefSensitivityLevel
                                    ]

    listFunctionsProcessDiagrams = [process_graphs.processDiagramSpectrumMask]

    # Lista de palabras clave del párrafo de la plantilla donde se insertarán las tablas
    listParagraphMarks = ["MARCADOR_POTENCIA_MAXIMA_CONDUCIDA",
                            "MARCADOR_ANCHURA_DE_BANDA_OCUPADA",
                            "MARCADOR_MASCARA_DE_TRANSMISION",
                            "MARCADOR_CANAL_ADYACENTE",
                            "MARCADOR_ERROR_DE_FRECUENCIA",
                            "MARCADOR_SENSIBILIDAD",
                            ]

    listImagesMarks = ["MARCADOR_IMAGENES_MASCARA"]