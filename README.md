# Parser XML para el CMW500

Programa que parsea un archivo XML generado por el instrumento CMW500 para distintas tecnologías celulares.
Recibe un archivo xml y una plantilla word, y devuelve un documento word con datos(tablas e imágenes)
generados a partir del XML